import os
import glob
import importlib.resources
from bs4 import BeautifulSoup

ROSTRUM_DIR = '_rostrum'
BROWSER_DIR = f'{ROSTRUM_DIR}/browser'

def make_dir(name):
    try:
        os.mkdir(name)
    except FileExistsError:
        pass

def make_link(source, dest):
    try:
        os.link(source, dest)
    except FileExistsError:
        pass

def copy_in():
    make_dir(ROSTRUM_DIR)
    make_dir(BROWSER_DIR)

    for name in (importlib.resources.files('rostrum') / 'browser' / 'files'
            ).glob('*'):
        with name.open('rb') as i:
            with open(os.path.join(
                    BROWSER_DIR,
                    os.path.basename(name),
                    ), 'wb') as o:
                o.write(bytes(i.read()))

    for name in glob.glob('bitmaps/*'):
        make_link(name,
                os.path.join(
                    BROWSER_DIR,
                    os.path.basename(name),
                    ))

def create_index(
        svg_filename = '0-14-clock.rostrum.svg',
        moves_filename = '0-14-clock.rostrum.json',
        ):
    template = (
            importlib.resources.files('rostrum') / 'browser' / 'index.html'
            ).open('r').read()

    index = BeautifulSoup(template,
            features='lxml',
            )

    if svg_filename is not None:
        with open(svg_filename, 'r') as f:
            svg = BeautifulSoup(f.read(),
                    features = 'lxml',
                    )

        svg_main = svg.find('svg')
        w = float(svg_main['width'])
        h = float(svg_main['height'])

        svg_main['width'] = '512'
        svg_main['height'] = str((h/w)*512)

        index.find(id='svg').append(svg_main)

    if moves_filename is not None:
        with open(moves_filename, 'r') as f:
            moves_text = f.read()

        moves_text = f'var moves={moves_text};'

        index.find(id='moves').append(moves_text)

    with open('_rostrum/browser/index.html', 'w') as out:
        out.write(str(index))

def main():
    copy_in()
    create_index()

if __name__=='__main__':
    main()
