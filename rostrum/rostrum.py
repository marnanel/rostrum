#!/usr/bin/env python

from bs4 import BeautifulSoup
import os
import sys
import subprocess
import glob
import pysrt
import re
import importlib.util
import PIL
from PIL import Image
import json
import cssutils
import importlib.resources
from rostrum.attrs import Attrs, TRANSFORM, TRANSFORM_KEYWORDS
import rostrum.actions
import rostrum.sprite
import rostrum.mover
import rostrum.shapes
import rostrum.canvas
from rostrum.util import *

PNG_TO_LST_BASE = re.compile(r'^(.*)-[0-9]+\.png$')
ASSIGNMENT = re.compile(
        r'^([a-zA-Z0-9_-]+)\.([a-zA-Z0-9_-]+)=([^;]+)(;(.*))?$')

DEBUG_OPTIONS  = set('make srt words'.split(' '))
STAGES_OPTIONS = set('generate rasterise composite film'.split(' '))

#########################################

def layers_from(svg):
    
    layers = [(x['inkscape:label'], (x, x.parent))
        for x in svg.find_all('g')
            if x.get('inkscape:groupmode', '')=='layer'
            and not (x.id and x.id.startswith('ignore-'))
            ]

    return layers

def make_link(src, dest):
    print(f"Hard linking: {src} -> {dest}... ", end='')

    if os.path.exists(dest):
        os.unlink(dest)

    os.link(src, dest)

class File:
    def __init__(self,
            ros,
            frame,
            cel,
            ext,
            ):
        self.ros = ros
        self.frame = frame
        self.cel = cel
        self.ext = ext

    @property
    def name(self):
        if self.ext=='video':
            return '%s.%s' % (self.ros.basename,
                    self.ros.filmext)
        elif self.cel is None:
            return '_rostrum/r.%06d.%s' % (self.frame, self.ext)
        else:
            return '_rostrum/r.%s.%06d.%s' % (self.cel, self.frame, self.ext)

    @property
    def basename(self):
        return os.path.basename(self.name)

    def __repr__(self):
        return self.name

    def but_with(self, **changes):
        values = {
                'ros': self.ros,
                'frame': self.frame,
                'cel': self.cel,
                'ext': self.ext,
                } | changes

        return self.__class__(**values)

class Rostrum:
    def __init__(self,
            basename,
            frames = None,
            debug = '',
            stages = '',
            filmext = 'mp4',
            dump_all_svg = False,
            jump = 1,
            cel = None,
            dry_run = False,
            sound = None,
            write_json = False,
            ):

        self.basename = basename

        self.debug = self._get_arg_items(
                debug, DEBUG_OPTIONS, 'none')
        self.stages = self._get_arg_items(
                stages, STAGES_OPTIONS, 'all')

        self.filmext = filmext
        self.dump_all_svg = dump_all_svg
        self.jump = 1
        self.cel = cel
        self.dry_run = dry_run
        self.sound = sound
        self.canvas = rostrum.canvas.Canvas()

        if write_json:
            self.json = {}
        else:
            self.json = None

        self.dirty_cels = set()
        self.movers = {}
        self.keyframes = {}

        self.load_svg()
        self.load_srt()
        self.load_commands()

        self._create_dirs()

        if frames is None:
            self.first_frame = 0
            self.last_frame = self.frame_count
        else:
            parts = frames.split('-')

            self.first_frame = int(parts[0])

            if len(parts)==1:
                self.last_frame = self.first_frame
            else:
                self.last_frame = int(parts[1])

        self.previous_svg_for_layer = {}

        self.expected_actions_total = 0

        self.svg_scale_factor = self._get_scale_factor()

    def _create_dirs(self):
        os.makedirs('_rostrum/cache', exist_ok=True)

    def _get_scale_factor(self):
        root = self.soup.find_all('svg')[0]
        viewbox = [float(n) for n in root['viewBox'].split(' ')]

        if viewbox[0:2]!=[0, 0]:
            raise ValueError("top left isn't 0,0; implement that bit")

        result = (
                viewbox[2] / float(root['width']),
                viewbox[3] / float(root['height']),
                )

        return result

    def _get_arg_items(self, arg, options, default):
        fields = arg.lower().split(',')

        if not fields or '' in fields:
            fields = [default]

        if 'all' in fields:
            return set([x for x in options])

        if 'none' in fields or '-' in fields:
            return set()

        if 'list' in fields:
            print('options are:')
            for entry in sorted(options):
                print(f'    {entry}')
            print('    all: select everything (the default)')
            print('   none: select nothing (synonym is "-")')
            sys.exit(1)

        result = set()
        errors = set()
        for field in fields:
            if field in options:
                result.add(field)
            else:
                errors.add(field)

        if errors:
            print('unknown options:')
            for entry in sorted(errors):
                print(f'    {entry}')
            sys.exit(255)

        return result

    def load_commands(self):

        self.commands_filename = f'{self.basename}.py'

        if not os.path.exists(self.commands_filename):
            with importlib.resources.open_text(rostrum,
                    'template.py.txt') as f:
                template = f.read()

            with open(self.commands_filename, 'w') as f:
                f.write(template)

            print(f'I have created the file {self.commands_filename} '
                    'in this directory.')

        spec = importlib.util.spec_from_file_location(
                name = self.basename.replace('-','_'),
                location = self.commands_filename,
                )
        self.commands = importlib.util.module_from_spec(spec)

        setattr(self.commands, 'shapes', rostrum.shapes.Shapes(self))
        setattr(self.commands, 'canvas', self.canvas)

        for name in ['sine', 'overshoot', 'linear']:
            setattr(self.commands, name, lambda n: (n, name))

        spec.loader.exec_module(self.commands)

    def load_svg(self):
        self.svg_filename = f'{self.basename}.rostrum.svg'

        if not os.path.exists(self.svg_filename):
            
            with importlib.resources.open_text(rostrum,
                    'template.svg') as f:
                template = f.read()

            with open(self.svg_filename, 'w') as f:
                f.write(template)

            print(f'I have created the file {self.svg_filename} '
                    'in this directory.')

        try:
            with open(self.svg_filename, 'r') as src:
                self.soup = BeautifulSoup(
                        src.read(),
                        'xml')
        except FileNotFoundError:
            sys.exit(254)

        for layer in layers_from(self.soup):
            layer[1][0]['style'] = 'display:inline'

    def load_srt(self):
        self.srt_filename = f'{self.basename}.srt'

        if not os.path.exists(self.srt_filename):
            with importlib.resources.open_text(rostrum,
                    'template.srt') as f:
                template = f.read()

            with open(self.srt_filename, 'w') as f:
                f.write(template)

            print(f'I have created the file {self.srt_filename} '
                    'in this directory.')

        srt = pysrt.open(self.srt_filename)
        self.srt = srt
        self.frame_count = int((as_ms(srt[-1].end)/1000)*FPS)

        self.layers_list = layers_from(self.soup)
        self.layers = dict(self.layers_list)
        self.current_frame = None
        self.action = None

        for line in srt:
            if line.text.startswith('#'):
                self.keyframes[line.text] = ms_to_frames(
                        as_ms(line.start))
                continue
            elif not line.text.startswith('*'):
                continue

            time = as_ms(line.start)

            command = line.text[1:].strip().replace(
                    '\n', ' ').split(' ')

            if 'srt' in self.debug:
                print('processing srt line:',
                        ('\n'+' '*24).join(command))

            for field in command:
                if 'srt' in self.debug:
                    print('processing:', field)

                match = ASSIGNMENT.match(field)

                if match is None:
                    raise ValueError(
                            f"I don't understand: {repr(field)}\n{line}")

                item_id = match.group(1)
                attrib = match.group(2)

                how = match.group(5)
                if how is None:
                    how = 'jump'

                self.at(
                        when = f'{time}ms',
                        id = item_id,
                        attr = attrib,
                        value = match.group(3),
                        how = how,
                        )

        if 'srt' in self.debug:
            for mover in self.movers.values():
                mover.dump()

    def at(self, when, id, value, attr=None, how='jump'):

        if isinstance(value, tuple):
            value, how = value

        if attr is None and ':' in id:
            id, attr = id.split(':')

        key = (id, attr)
        if key not in self.movers:
            self.movers[key] = rostrum.mover.Mover(
                    ros = self,
                    id = id,
                    attr = attr,
                    )

        if when is None:
            pass
        elif isinstance(when, int):
            pass
        elif when.startswith('#'):
            if when not in self.keyframes:
                raise ValueError(f"{when} is not the name of a keyframe")
            if 'srt' in self.debug:
                print('(%s is %sf)' % (when, self.keyframes[when]))
            when = self.keyframes[when]
        elif when.endswith('ms'):
            when = int(when[:-2])
        elif when.endswith('s'):
            when = int(when[:-1])*1000
        elif when.endswith('f'):
            when = int(when[:-1]*1000*FPS)
        else:
            raise ValueError(f"Don't understand time value: {when}")

        self.movers[key].add_point(
                time = when,
                value = value,
                how = how,
                )

    def setup_frame(self, frame):

        if frame==self.current_frame:
            return

        if self.current_frame is not None and frame<self.current_frame:
            raise ValueError(
                    f"frame number went backwards: "
                    f"{self.current_frame} to {frame}"
                    )

        self.current_frame = frame
        time = frame_to_time_str(frame)

        if frame==self.first_frame:
            # all cels are dirty
            self.dirty_cels = set([x[0] for x in self.layers_list])
        else:
            self.dirty_cels = set()

        if self.debug:
            print()
            print(f'== {frame} ({time}) ==')

        self.current_frame = frame

        for routine in dir(self.commands):
            if not routine.startswith('move_'):
                continue
            
            getattr(self.commands, routine)(
                    frame = frame,
                    ros = self,
                    )

        for mover in self.movers.values():
            mover.move(frame)

        if self.dump_all_svg or (self.json is not None and frame==0):
            with open(self.filename(
                frame, 'svg', cel=None).name, 'w') as out:

                out.write(str(self.soup))

    def select_cel(self, layer_name):

        if layer_name is not None and layer_name not in self.dirty_cels:
            raise ValueError("You can't select a clean cel: {layer_name}")

        for name, v in self.layers.items():
            layer, parent = v
            if layer in parent:
                layer.extract()

        for name, v in self.layers.items():
            layer, parent = v

            if not layer_name or name==layer_name:
                parent.append(layer)

    def get(self, id, default=None):

        result = rostrum.sprite.Sprite.from_name(ros=self, name=id)

        return result or default

    def scene(self):
        return rostrum.sprite.Sprite(self.soup)

    def __getitem__(self, id):
        result = self.get(id)
        if result is None:
            raise KeyError(id)
        return result

    def run(self):
        self.action = rostrum.actions.Film(
                ros = self,
                )

        self.action.run()

        self.teardown()

    def teardown(self):
        if self.json is not None:

            for item in self.json:
                previous = {}
                for i in range(int(max(self.json[item]))+1):
                    i_key = i

                    if i_key in self.json[item]:

                        attrs = self.json[item][i_key]

                        if 'x' in attrs or 'y' in attrs:
                            attrs['xy'] = '%s,%s' % (
                                    attrs.get('x', "0.0"),
                                    attrs.get('y', "0.0"),
                                    )
                            if 'x' in attrs:
                                del attrs['x']
                            if 'y' in attrs:
                                del attrs['y']

                        previous = attrs

                    else:
                        pass # self.json[item][i_key] = previous

            with open(f'{self.basename}.rostrum.json', 'w') as f:
                json.dump(self.json, fp=f, indent=2, sort_keys=True)

    def svg_contents(self):
        return str(self.soup)

    def filename(self, frame, ext, cel=None):
        return File(
                ros=self,
                frame=frame,
                ext=ext,
                cel=cel,
                )

    @property
    def progress(self):
        if self.action is None:
            return (0, 0)

        return self.action.progress

    def raster_to_svg_coords(self, raster):
        result = (
                raster[0]*self.svg_scale_factor[0],
                raster[1]*self.svg_scale_factor[1],
                )
        return result

    def add_to_json(self, id, k, v):
        if self.json is None:
            return
        
        if k=='transform':
            return

        n = '%06d' % (self.current_frame,)

        if id not in self.json:
            self.json[id] = {}

        if n not in self.json[id]:
            self.json[id][n] = {}

        self.json[id][n][k] = v
