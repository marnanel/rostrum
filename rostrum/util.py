import math

FPS = 25

def ms_to_frames(ms):
    return int((ms/1000)*FPS)

def as_ms(t):
    return t.minutes*60000+t.seconds*1000+t.milliseconds

def deg_to_rad(deg):
    return deg*(1/360)*2*math.pi

def rad_to_deg(rad):
    return (rad/(2*math.pi))*360

def show(item, whether):
    if whether:
        item['display'] = 'inline'
    else:
        item['display'] = 'none'

def frame_to_time_str(f):
    ms = int((f/FPS)*1000)
    time = '%02dm%02d.%03ds' % (
            int(ms/60000),
            int(ms/1000)%60,
            ms%1000,
            )
    return time
