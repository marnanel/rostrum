from bs4 import BeautifulSoup
import argparse
import os
import pysrt
import glob
import re
import collections
import math
from rostrum.util import ms_to_frames, as_ms, deg_to_rad
import rostrum.sprite

PREFIX = 'bywords-'

bywords_groups = None

class BywordsGroup:
    def __init__(self, ros, g):

        self.name = g['id'][len(PREFIX):]

        self.spans = []
        for tagname in ['tspan', 'textPath']:
            self.spans.extend([(
                rostrum.sprite.Sprite(
                    ros=ros,
                    source=x),
                x.text.split()) for x in g.find_all(tagname)
                if x.contents and isinstance(x.contents[0], str)])

        words = []
        for s, w in self.spans:
            words.extend(w)

        self.rostrum = ros

        if 'words' in self.rostrum.debug:
            print('Spans:', self.spans)
            print('Words:', words)

        self.phrase = ' '.join(words).lower()
        self.phrase = re.sub(r'[^a-z0-9 ]', '', self.phrase).strip()

        srt_line = None
        for line in ros.srt:
            text = line.text.lower()
            text = re.sub(r'[^a-z0-9 ]', '', text)
            if text.startswith(self.phrase):
                if srt_line is not None:
                    raise ValueError(f"{self.phrase} exists more than once")
                srt_line = line

        if not srt_line:
            raise ValueError(f"{self.phrase}: not found")

        length_in_ms = as_ms(srt_line.end)-as_ms(srt_line.start)
        total_letters = sum([len(w)+1 for w in words])
        per_letter = length_in_ms/total_letters

        share = [
                ms_to_frames(int(per_letter*(len(word)+1)))
                for word in words
                ]

        share.insert(0, 0)

        time = ms_to_frames(as_ms(srt_line.start))
        self.frames = []
        for n in share:
            self.frames.append(time)
            time += n

    def make_change(self, frame):

        if 'words' in self.rostrum.debug:
            print(self.frames, self.phrase)

        if not self.frames:
            return

        needed = len([n for n in self.frames if n<frame])

        count = 0
        for span, words in self.spans:

            if count+len(words)<needed:
                pass
            elif count<needed:
                words = words[:needed-count]
            else:
                words = []

            span.string = ' '.join(words)

            count += len(words)

    def __repr__(self):
        return self.name

def load_groups(ros):
    global bywords_groups

    if bywords_groups:
        return

    bywords_groups = []
    for g in ros.soup.find_all('g'):
        if 'words' in ros.debug:
            print(g.get('id','-'))
        if g.get('id', '').startswith(PREFIX):
            bywords_groups.append(
                    BywordsGroup(ros, g))

    if 'words' in ros.debug:
        print(bywords_groups)

def move_bywords(frame, ros):

    global bywords_groups

    if bywords_groups is None:
        load_groups(ros)

    for bg in bywords_groups:
        bg.make_change(frame)
