
class Shapes:

    def __init__(self, ros):
        self.ros = ros

    def __getattr__(self, n):
        return Shape(ros=self.ros, shape=n)

    def __getitem__(self, n):
        return Shape(ros=self.ros, shape=n)

class Shape:

    def __init__(self, ros, shape):
        self._ros = ros
        self._shape = shape

        if not self.item:
            raise KeyError(f"there is no item called {self._shape}")

    @property
    def item(self):
        return self._ros.soup.find(id=self._shape)

    def __getattr__(self, k):
        if k in ('ros', 'shape'):
            return getattr(self, f'_{k}')

        return Attrib(ros=self._ros, shape=self._shape, attr=k)

    def __setattr__(self, k, v):
        if k.startswith('_'):
            object.__setattr__(self, k, v)
            return

        self._ros.at(
                when=None,
                value=v,
                id=self._shape,
                attr=k,
                )

class Attrib:

    def __init__(self, ros, shape, attr):
        self.ros = ros
        self.shape = shape
        self.attr = attr

    def __setitem__(self, when, value):

        self.ros.at(
                when=when,
                value=value,
                id=self.shape,
                attr=self.attr,
                )
