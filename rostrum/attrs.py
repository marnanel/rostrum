import re
import cssutils
import rostrum.sprite

TRANSFORM = 'transform'
STYLE = 'style'

TRANSFORMS = {
        'x': 'translate',
        'y': 'translate',
        'translate': 'translate',
        'scale': 'scale',
        'rotate': 'rotate',
        }
TRANSFORM_RE = re.compile(r'([a-z]+)\(([^)]*)\)')
TRANSFORM_KEYWORDS = set(['x', 'y', 'scale', 'scalex', 'scaley', 'rotate'])

newgroup_count = 0

class Attrs(dict):

    def write(self):

        for k, v in self.items():
            self._ros.add_to_json(
                    self._target_item['id'],
                    k, v)

        def appropriate_item(k):
            transform_target = TRANSFORMS.get(k, None)

            if transform_target is not None:
                return (
                        'transform',
                        self._ros.get(
                            f'{self._target_item["id"]}-{transform_target}',
                            ),
                        )
            else:
                return (
                        k,
                        self._target_item,
                        )

        def put(k, v):
            k, item = appropriate_item(k)
            item.__setattr__(k, v)

        def translate(x, y):
            k, thing = appropriate_item('x')

            try:
                existing = TRANSFORM_RE.match(thing['transform'])
            except KeyError:
                existing = None

            if existing is not None:
                eg = existing.groups()
                if eg[0]=='translate':
                    xy = eg[1].split(',')

                    if x is None:
                        x = xy[0]

                    if y is None:
                        y = xy[1]

                if thing==self._target_item:
                    del thing['transform']

            x = x or 0
            y = y or 0

            thing.__setitem__('transform', f'translate({x},{y})')

        if self._from=='transform':
            scale = []

            if 'scale' in self:
                scalex = scaley = self.get('scale')
            else:
                scalex = self.get('scalex', 1.0)
                scaley = self.get('scaley', 1.0)

            put('scale', f'scale({scalex}, {scaley})')

            if 'x' in self or 'y' in self:
                translate(
                        x = self.get('x', None),
                        y = self.get('y', None),
                        )

            rotate = self.get('rotate', 0)
            put('rotate', f'rotate({rotate})')

        elif self._from=='style':
            put('style', ';'.join([
                    f'{f}: {v}' for f,v in self.items()]))
        else:
            for f,v in self.items():
                put(f, v)

    def _set_target_item(self, item):
        self._target_item = rostrum.sprite.Sprite.maybe_wrap(item)

    @classmethod
    def from_style(cls, ros, item):

        style = item.get(STYLE, '')

        result = {}

        if style:
            p = cssutils.parseStyle(style)
            for item in p:
                result[item.name] = item.value

        result = cls(result)
        result._target_item = rostrum.sprite.Sprite.maybe_wrap(
                ros=ros,
                something=item,
                )
        result._from = 'style'
        result._ros = ros
        return result

    @classmethod
    def from_transform(cls, ros, item):

        if '-' in item.get('id', ''):
            raise ValueError()

        result = cls()
        result._target_item = rostrum.sprite.Sprite.maybe_wrap(
                ros=ros,
                something=item,
                )
        result._from = 'transform'
        result._ros = ros
        return result

    @classmethod
    def from_attr(cls, ros, item, attr):

        result = cls()
        result[attr] = item.get(attr, '')
        result._target_item = rostrum.sprite.Sprite.maybe_wrap(
                ros=ros,
                something=item,
                )
        result._from = attr
        result._ros = ros

        if attr!='display':
            raise ValueError()

        return result
