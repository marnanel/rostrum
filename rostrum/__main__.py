import os
import argparse
import rostrum.rostrum

def main():

    parser = argparse.ArgumentParser(
        description='make an animation scene')
    parser.add_argument(
            '--dry-run', '-d',
            action='store_true',
            help='just show what will be done')
    parser.add_argument(
            '--sound', '-s',
            action='store',
            help='use this sound file for the film')
    parser.add_argument(
            '--stages', '-S',
            type=str,
            default='',
            help=(
                'comma-separated stages to run; -S list for a list; '
                'default is all of them'),
            )
    parser.add_argument(
            '--cel', '-C',
            type=str,
            default=None,
            help=(
                'cel to produce and make the film out of; if you don\'t '
                'specify this, you will get a composited version of '
                'all of them'),
            )
    parser.add_argument(
            '--frames', '-f',
            action='store',
            default=None,
            help=('which frames to produce, e.g. "6" or "200-300"; '
                'default is all of them'),
            )
    parser.add_argument(
            '--filmext', '-F',
            action='store',
            default='mp4',
            help='what kind of film you want')
    parser.add_argument(
            '--debug', '-D',
            type=str,
            default='',
            help=(
                'show various debug things; -D list for a list; '
                'default is none of them'),
            )
    parser.add_argument(
            '--json', '-j',
            action='store_true',
            help=(
                'write out movements to <basename>.json '
                '(usefully combined with -d)'
                ),
            )
    parser.add_argument(
            '--jump', '-J',
            action='store',
            type=int,
            default=1,
            help='step between generated images (e.g. 10 for every 10th one)')
    parser.add_argument(
            '--dump-all-svg',
            action='store_true',
            help='write out the current SVG on each step (as r.%%06d.svg)')

    args = parser.parse_args()

    base = os.path.basename(os.getcwd())
    r = rostrum.rostrum.Rostrum(
            base,
            frames = args.frames,
            debug = args.debug,
            stages = args.stages,
            filmext = args.filmext,
            dump_all_svg = args.dump_all_svg,
            jump = args.jump,
            cel = args.cel,
            dry_run = args.dry_run,
            sound = args.sound,
            write_json = args.json,
            )
    r.run()

if __name__=='__main__':
    main()
