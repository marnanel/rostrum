from bs4 import BeautifulSoup
import os
import sys
import subprocess
import glob
import pysrt
import re
import importlib.util
import math
import PIL
from PIL import Image
import cssutils
from rostrum.attrs import Attrs, TRANSFORM, TRANSFORM_KEYWORDS
from rostrum.util import FPS
import hashlib

BITMAPS_DIR = 'bitmaps'
TEMP_SVG = '.rostrum-temp.svg'

IMAGE_DIR = '_rostrum'
CACHE_DIR = '_rostrum/cache'

COMPOSITED_ID = '_composited'

class Action:

    ext = None
    cacheable = True

    previous_frame_hash_for_cel = {}

    @classmethod
    def for_file(cls,
            ros,
            source,
            depth=0,
            ):

        if isinstance(source, str):
            # files identified by filenames as strings can't be generated
            return []

        result = []

        for name, item in globals().items():

            htc = getattr(item, 'how_to_create', None)

            if htc is None:
                continue

            if htc==cls.how_to_create:
                # it's us (or they haven't overridden it)
                continue

            found = htc(
                    ros=ros,
                    source=source,
                    )

            if found and found.ros is None:
                found.ros = ros

            if found is not None:
                result.append(found)

        if not result:
            print("warning: I don't know how to create {source}")

        return result

    def cache_name(self, plaintext):
        """
        Finds the name of a file to render to, and whether that file exists.

        Sets self.hash to the hash of "plaintext".
        (Currently we're using md5.)

        Args:
            plaintext (str): text representing the input to this file

        Returns:
            (filename, whether it exists)
        """

        key = f"{self.name} {self.cel}"

        if plaintext is None:
            self.hash = self.previous_frame_hash_for_cel[key]
        else:
            self.hash = hashlib.md5(bytes(plaintext, 'utf-8')).hexdigest()
            self.previous_frame_hash_for_cel[key] = self.hash

        filename = os.path.join(CACHE_DIR, self.hash)

        return (filename, os.path.exists(filename))

    @classmethod
    def how_to_create(cls, source, **kwargs):
        raise NotImplementedError()

    def __init__(self,
            frame,
            target,
            sources,
            cel = None,
            ros = None,
            ):

        self.name = self.__class__.__name__.lower()
        self.ros = ros
        self.frame = frame
        self.target = target
        self.sources = sources
        self.actions = None
        self.cel = cel
        self.depth = 0

        self.hash = 0

        self.our_actions_so_far = 0
        self.our_actions_total = 1

        self.sub_actions_so_far = 0

    def run(self):

        self.selected_stage = self.name in self.ros.stages
        self.ros.setup_frame(self.frame)

        prefix = ' '*self.depth

        clean = True
        parent_of_unselected_stage = False

        self.ros.expected_actions_total += len(self.sources)

        self.actions = []

        for source in self.sources:

            if hasattr(source, 'name'):
                source_name = source.name
            else:
                source_name = source

            if not isinstance(source, str):
                source.depth = self.depth+1
                source.ros = source.ros or self.ros

            self.actions.extend(Action.for_file(
                    ros=self.ros,
                    source=source,
                    ))

        for action in self.actions:
            action.run()

        plaintext = self.get_plaintext()

        if self.cacheable:
            cache_name, exists = self.cache_name(plaintext)

            if exists:
                self.link(
                        cache_name,
                        self.target.name,
                        )
                return
        else:
            cache_name = None

        print('%4d %s... ' % (self.ros.current_frame, self.name),
                end='', flush=True)

        if self.ros.dry_run:
            print('dry run.')
            return

        self._do_the_thing(
                filename=cache_name,
                plaintext=plaintext,
                )

        if self.target.name.startswith('_rostrum'):
            self.link(
                    cache_name,
                    self.target.name,
                    )
            print("done.")

        # if it doesn't start with "_rostrum", it involved printing loads of
        # stuff anyway. So it doesn't matter that we don't print a final
        # end-of-line character.

    def get_plaintext(self):
        return '\x00'.join([x.hash for x in self.actions])

    def _do_the_thing(self, filename, plaintext):
        raise NotImplementedError()

    def __str__(self):
        return f'{self.__class__.__name__.lower()}: {self.frame}'

    def display(self, message, prefix='',
            add_actions = 1,
            end = '\n'):

        progress = self.ros.progress

        if progress[0]>progress[1]:
            raise ValueError(f"{self}: {progress} is top-heavy")

        print("%6d/%6d %3d%%:%-8s: %30s: %s" % (
            progress[0], progress[1],
            (progress[0] / progress[1])*100,
            (' '*self.depth)+prefix,
            self.target.basename,
            message,
            ),
            end=end,
            flush=(end==''),
            )

        self.our_actions_so_far += add_actions

    def is_dirty(self, source, target):

        def show_trace(result, message):
            if 'make' not in self.ros.debug:
                return result

            prefix = ' ' * self.depth

            if result:
                prefix += 'y'
            else:
                prefix += ':'

            self.display(
                    prefix = prefix,
                    message = f'{target} <- {source} : {message}',
                    add_actions = 0,
                    )

            return result

        if not os.path.exists(source):
            return show_trace(True, "no source")

        if not os.path.exists(target):
            return show_trace(True, "no target")

        source_stat = os.stat(source)
        target_stat = os.stat(target)

        if source_stat.st_mtime > target_stat.st_mtime:
            return show_trace(True, f"source is newer {source_stat.st_mtime} vs {target_stat.st_mtime}")

        return show_trace(False, "we're up to date")

    def link(self, src, dest):
        if not os.path.exists(src):
            raise ValueError(f"Link from {src} to {dest}: src doesn't exist")

        if os.path.exists(dest):
            inodes = [os.stat(f).st_ino for f in [src, dest]]
            if inodes[0]==inodes[1]:
                # already linked
                return

            os.unlink(dest)

        os.link(src, dest)

    @property
    def progress(self):

        actions_so_far = self.our_actions_so_far + self.sub_actions_so_far
        our_actions_remaining = self.our_actions_total - \
                self.our_actions_so_far

        if self.our_actions_so_far==0:
            expected_actions_total = actions_so_far + our_actions_remaining
        else:
            expected_actions_total = actions_so_far + \
                    our_actions_remaining * (1+(
                    self.sub_actions_so_far / self.our_actions_so_far))

        return (actions_so_far, expected_actions_total)

class Generate(Action):

    ext = 'svg'

    @classmethod
    def how_to_create(cls, source, **kwargs):
        if source.ext=='svg' and source.cel is not None:
            return cls(
                    frame=source.frame,
                    cel=source.cel,
                    **kwargs)

        return None

    def __init__(self, frame,
            cel,
            ros,
            **kwargs):

        target = ros.filename(
                frame=frame,
                cel=cel,
                ext='svg',
                )

        super().__init__(
                target = target,
                frame = frame,
                cel = cel,
                sources = [
                    ros.svg_filename,
                    ],
                **kwargs,
                )

    def get_plaintext(self):
        if self.cel not in self.ros.dirty_cels:
            return None

        self.ros.select_cel(self.cel)
        result = self.ros.svg_contents()
        self.ros.select_cel(None)

        return result

    def _do_the_thing(self, filename, plaintext):
        with open(filename, 'w') as out:
            out.write(plaintext)

class Rasterise(Action):

    ext = 'png'

    @classmethod
    def how_to_create(cls, source, **kwargs):
        if source.ext=='png' and source.cel is not None and \
                source.cel!=COMPOSITED_ID:
            return cls(
                    frame=source.frame,
                    cel=source.cel,
                    **kwargs)

        return None

    def __init__(self, frame,
            cel,
            ros,
            **kwargs):

        source = ros.filename(
                frame=frame,
                cel=cel,
                ext='svg',
                )

        target = ros.filename(
                frame=frame,
                cel=cel,
                ext='png',
                )

        super().__init__(
                target = target,
                ros = ros,
                frame = frame,
                cel = cel,
                sources = [
                    source,
                    ],
                **kwargs,
                )

    def _do_the_thing(self, filename, plaintext):

        temp_svg_filename = os.path.join(BITMAPS_DIR, TEMP_SVG)

        try:
            os.unlink(temp_svg_filename)
        except FileNotFoundError:
            pass

        os.link(self.sources[0].name, temp_svg_filename)

        args = [
            '/usr/bin/rsvg-convert',
            TEMP_SVG,
            '-f' ,'png',
            '-o', os.path.abspath(filename),
            ]

        if self.ros.canvas.width is not None:
            args.append(f'--width={self.ros.canvas.width}')
        if self.ros.canvas.height is not None:
            args.append(f'--height={self.ros.canvas.height}')

        returncode = subprocess.call(
            args,
            cwd = BITMAPS_DIR,
            )

        os.unlink(temp_svg_filename)

        if returncode!=0:
            print(" failed.")
            sys.exit(returncode)

    def __str__(self):
        return f'{super().__str__()}: {self.cel}'

class Composite(Action):

    ext = 'png'

    @classmethod
    def how_to_create(cls, source, **kwargs):
        if source.ext=='png' and source.cel==COMPOSITED_ID:
            return cls(
                    frame=source.frame,
                    **kwargs)

        return None

    def __init__(self, frame,
            ros,
            **kwargs,
            ):

        sources = [
                ros.filename(
                    frame=frame,
                    cel=cel,
                    ext='png',
                    )
                for cel in ros.layers.keys()]
        target = ros.filename(
                frame = frame,
                cel = COMPOSITED_ID,
                ext='png')

        sources = list(sources)

        super().__init__(
                target = target,
                ros = ros,
                frame = frame,
                cel = COMPOSITED_ID,
                sources = sources,
                **kwargs,
                )

    def _do_the_thing(self, filename, plaintext):

        result = Image.open(self.sources[0].name).convert("RGBA")

        for cel in self.sources[1:]:
            other = Image.open(cel.name).convert("RGBA")

            result = Image.alpha_composite(result, other)

        result.save(filename, format='png')

class Mainlink(Action):

    ext = 'png'

    @classmethod
    def how_to_create(cls, source, **kwargs):
        if source.ext=='png' and source.cel is None:
            return cls(
                    frame=source.frame,
                    **kwargs)

        return None

    def __init__(self, frame,
            ros,
            **kwargs,
            ):

        cel = ros.cel or COMPOSITED_ID

        sources = [
                ros.filename(frame=frame, cel=cel, ext='png'),
                ]

        target = ros.filename(frame=frame, cel=None, ext='png')

        super().__init__(
                target = target,
                ros = ros,
                frame = frame,
                cel = None,
                sources = sources,
                **kwargs,
                )

    def _do_the_thing(self, filename, plaintext):
        try:
            os.unlink(filename)
        except FileNotFoundError:
            pass

        os.link(self.sources[0].name, filename)

class Film(Action):

    ext = None # caching wouldn't make sense here
    cacheable = False

    @classmethod
    def how_to_create(cls, source, **kwargs):
        if source.ext=='video':
            return cls(**kwargs)

        return None

    def __init__(self,
            ros,
            **kwargs,
            ):

        target = ros.filename(
                cel = None,
                frame = 0,
                ext = 'video',
                )

        sources = [
                ros.filename(
                    cel=None,
                    frame=frame,
                    ext = 'png',
                    )
                for frame in range(
                    ros.first_frame,
                    ros.last_frame+1,
                    ros.jump)
                ]

        super().__init__(
                target = target,
                ros = ros,
                frame = 0,
                cel = None,
                sources = sources,
                **kwargs,
                )

    def _do_the_thing(self, filename, plaintext):

        format_string = self.ros.filename(
                0, 'png').name.replace('000000', '%06d')

        if self.ros.sound:
            sound = ['-i', self.ros.sound]
        else:
            sound = []

        returncode = subprocess.call([
            '/usr/bin/ffmpeg',
            '-r', str(FPS),
            '-i', format_string,
            *sound,
            '-movflags', '+faststart',
            '-c:v', 'libx264', '-preset', 'superfast',
            '-crf', '18',
            '-pix_fmt', 'yuv420p',
            '-y',
            # ignore the filename
            self.target.name,
            ])

        if returncode!=0:
            print(" failed.")
            sys.exit(returncode)

        print('done!')
