import rostrum

class Sprite:
    # the order is important for their independence
    WRAPPED = ['scale', 'rotate', 'translate']

    _added_id_count = 0

    def __init__(self, ros, source):
        self._ros = ros

        self._source = {
                None: source,
                }

        self._cel = source

        if source.get('id', None) is None:
            source['id'] = f'rostrum{self._added_id_count}'
            self._added_id_count += 1

        source_name = source['id']
        for name in self.WRAPPED:
            self._source[name] = ros.soup.get(f'{source_name}-{name}')

        while self._cel and self._cel.name!='svg' and \
                self._cel.get('inkscape:groupmode','')!='layer':

                self._cel = self._cel.parent

        if not self._cel or self._cel.name=='svg':
            self._cel = None
        else:
            self._cel = self._cel['inkscape:label']

    def _source_item_for_attr(self, attr):
        name = rostrum.attrs.TRANSFORMS.get(attr, None)
        return self._source[name]

    def __setattr__(self, k, v):
        if k.startswith('_'):
            object.__setattr__(self, k, v)
            return

        self._ros.dirty_cels.add(self._cel)
        item = self._source_item_for_attr(k)

        if k in ('string'):
            setattr(item, k, v)
        else:
            item[k] = v
        self._ros.add_to_json(self._source[None]['id'], k, v)

    def __getattr__(self, k):
        item = self._source_item_for_attr(k)
        if k in ('string'):
            result = item[k]
        else:
            result = getattr(item, k)
        return result

    def __getitem__(self, k):
        item = self._source_item_for_attr(k)
        result = item[k]
        return result

    def get(self, attr, default):
        result = self._source[None].get(attr, default)
        return result

    def __setitem__(self, k, v):
        item = self._source_item_for_attr(k)
        item[k] = v
        self._ros.dirty_cels.add(self._cel)
        self._ros.add_to_json(self._source[None]['id'], k, v)

    def __delitem__(self, k):
        item = self._source_item_for_attr(k)
        del item[k]

    @classmethod
    def maybe_wrap(cls, ros, something):
        if isinstance(something, cls):
            return something
        else:
            return cls(ros=ros, source=something)

    def __repr__(self):
        if self.cel:
            cel_name = self.cel.name
        else:
            cel_name = '<no cel>'

        return 'sprite;%s;%s' % (
                cel_name,
                self._source[None],
                )

    @classmethod
    def without_suffix(cls, name):
        return name.split('-')[0]

    @classmethod
    def ensure_wrapped(cls, ros, name):
        basename = cls.without_suffix(name)

        thing = ros.soup.find(id=basename)

        for wrapper in cls.WRAPPED:

            wrapper_name = f"{basename}-{wrapper}"

            if not ros.soup.find_all(id=wrapper_name):
                new_tag = ros.soup.new_tag('g')
                new_tag['id'] = wrapper_name
                thing.replace_with(new_tag)
                new_tag.append(thing)

                thing = new_tag

    @classmethod
    def from_name(cls, ros, name):

        cls.ensure_wrapped(ros, name)
        item = ros.soup.find(id=name)

        if not item:
            raise KeyError(name)

        return cls(ros, item)
