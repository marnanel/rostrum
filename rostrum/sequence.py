import re
import os
import itertools

DIGITS = re.compile(r"(\d+)")

class MoveSequence:
    def __init__(self):
        self.sequences = []
        self.current_frame = 0

    def add(self, name, seq):
        self.sequences.append( {
            'name': name,
            'element': None,
            'seq': seq,
            'iter': itertools.cycle(iter(seq)),
            'width': 0,
            } )

    def _resolve_names(self, ros):

        for seq in self.sequences:
            if seq['element'] is not None:
                continue

            element = ros[seq['name']]
            if element.name!='image':
                images = element.find_all('image')
                if not images:
                    raise ValueError(f"{element} was not an image")
                element = images[0]

            seq['element'] = element

            digits = DIGITS.findall(element['xlink:href'])
            if not digits:
                raise ValueError(f"{seq} has no digits in its href")

            seq['width'] = len(digits[0])

    def __call__(self, frame, ros):
        if frame!=self.current_frame:
            raise ValueError("frames have got out of order")
        self.current_frame += 1

        if frame==0:
            self._resolve_names(ros)

        for seq in self.sequences:
            seq_frame = next(seq['iter'])
            element = seq['element']

            try:
                href = element['xlink:href']
            except KeyError:
                raise ValueError(f"No href found in {element}")

            digits = DIGITS.findall(href)
            if not digits:
                raise ValueError(f"{element} has no digits in its href")

            href = DIGITS.sub(
                    '%0*d' % (seq['width'], seq_frame,),
                    href)

            if not os.path.exists(href):
                raise ValueError(f"sequence implies {href}, "
                        "which doesn't exist")

            element['xlink:href'] = href
            element['sodipodi:absref'] = os.path.abspath(href)

move_sequence = MoveSequence()

add = move_sequence.add
