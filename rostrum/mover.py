import math
from rostrum.attrs import Attrs, TRANSFORM_KEYWORDS
from rostrum.util import FPS, frame_to_time_str
import rostrum.sprite

class Mover:
    def __init__(self, ros, id, attr):
        self.ros = ros

        self.item = ros[id]
        if self.item is None:
            raise KeyError(f"there is no item called {id}")
        self.attr = attr

        if attr=='display':
            self.item_attrs = Attrs.from_attr(self.ros, self.item, attr)
        elif attr in TRANSFORM_KEYWORDS:
            self.item_attrs = Attrs.from_transform(self.ros, self.item)
        else:
            self.item_attrs = Attrs.from_style(self.ros, self.item)

        # Sometimes, Attrs will wrap the item in a group.
        # So we must ask what the item is now.
        self.item = self.item_attrs._target_item

        if attr in self.item_attrs:
            first = self.item_attrs[attr]
            self.points = { 0: first, }
        else:
            self.points = {}

        self.frame_count = self.ros.frame_count

    def _set_attribute(self, value):
        if self.item_attrs._from=='transform':
            self.item_attrs[self.attr] = float(value)

            if self.attr=='scale':
                try:
                    del self.item_attrs['scalex']
                except KeyError:
                    pass

                try:
                    del self.item_attrs['scaley']
                except KeyError:
                    pass

        elif self.item_attrs._from == 'display':

            if value:
                self.item_attrs[self.attr]='inline'
            else:
                self.item_attrs[self.attr]='none'

        else:
            self.item_attrs[self.attr] = value

        self.item_attrs.write()

    def latest_time(self):
        if not self.points:
            raise ValueError(
                    f"{self.item} = {self.attr}: "
                    "Can't get latest time because the SVG didn't "
                    "contain an initial value, and we didn't yet add one "
                    'with ";jump".')

        return max(self.points.keys())

    def add_point(self, time, value, how='jump',
            message = 'add point'):

        if not isinstance(value, str):
            value = str(round(value, 3))

        if time is None:
            # do it right now
            assert how=='jump'
            self._set_attribute(value)
            if 'srt' in self.ros.debug:
                print('%20s: set to %s' % (self.name, value))
            return

        if 'srt' in self.ros.debug:
            print('%20s: %15s: %4s (%8s) %6s: %s' % (
                self.name,
                message,
                time,
                frame_to_time_str(time),
                value, how))

        if how=='jump':
            self.points[time] = value
            return

        time_start = self.latest_time()
        time_end   = time
        time_range = time_end - time_start

        if time_range<2:
            self.points[time] = value
            return

        value_start = float(self.points[time_start])
        value_end   = float(value)
        value_range = value_end-value_start

        if how=='sine':

            for i in range(1, time_range+1):

                angle = (i/time_range)*(math.pi/2)

                self.add_point(
                    time = i+time_start,
                    value = value_start + math.sin(angle)*value_range,
                    message = 'sine ->',
                    )

            self.add_point(
                time = time,
                value = value,
                message = 'final',
                )

        elif how=='overshoot':
            # FIXME this should be configurable

            desc = (
                    f' {time_start}={value_start} to '
                    f'{time_end}={value_end}'
                    )

            self.add_point(
                    int(time_start + time_range * 0.9),
                    value = value_start + value_range * 1.05,
                    how='sine',
                    message='-- in'+desc,
                    )

            self.add_point(
                    time, value,
                    how='sine',
                    message='-- out'+desc,
                    )

        elif how=='linear':

            value_start = float(self.points[time_start])
            value_end = float(value)
            value_range = value_end - value_start

            step = value_range / time_range

            for i in range(1,time_range):
                self.add_point(
                        time = i+time_start,
                        value = value_start + step*i,
                        message = 'linear ->',
                        )

            self.add_point(
                time = time,
                value = value,
                message = 'final',
                )

        else:
            raise ValueError(how)

    def dump(self):
        if 'srt' not in self.ros.debug:
            return

        print(f"== {self.name}")
        for f,v in sorted(self.points.items()):
            time = frame_to_time_str(f)
            print("    %10s %5s -> %s=%s" % (time, f, self.attr, v))
        print()

    @property
    def name(self):
        return f"{self.item['id']}.{self.attr}"

    def move(self, frame):
        if frame in self.points:
            self._set_attribute(self.points[frame])

            if 'srt' not in self.ros.debug:
                return

            print("Move: %s.%s: %s" % (
                    self.item['id'], self.attr, self.item_attrs))

    def __repr__(self):
        return f'[mover for "{self.attr}" of "{self.item.get("id","?")}"]'
