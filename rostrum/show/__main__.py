import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf

class RostrumWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Rostrum")
        self.set_default_size(400, 400)

        #self.controls = Gtk.HBox(spacing=6)

        self.image = Gtk.Image()
        self.image.connect('draw', self.scale_image, self)
        self.image_filename = '_rostrum/r.000000.png'

        self.set_image()

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.button_zoom_in = Gtk.Button(label="Zoom-In")
        self.button_zoom_out = Gtk.Button(label="Zoom-Out")      

        scrolledwindow = Gtk.ScrolledWindow()
        self.viewport = Gtk.Viewport()
        self.drawing_area = Gtk.DrawingArea()
        self.drawing_area.set_size_request(
                              self.pixbuf.get_width(), self.pixbuf.get_height())
        self.drawing_area.set_events(Gdk.EventMask.ALL_EVENTS_MASK)

        self.viewport.add(self.drawing_area)
        scrolledwindow.add(self.viewport)
        box.pack_start(self.button_zoom_in, False, True, 0)
        box.pack_start(self.button_zoom_out, False, True, 0)
        box.pack_start(scrolledwindow, True, True, 0)
        self.add(box)

        self.connect("destroy", Gtk.main_quit)

        self.drawing_area.connect("draw", self.on_drawing_area_draw)

        self.show_all()

    def set_image(self):
        self.pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.image_filename)

    def on_drawing_area_draw(self, drawable, cairo_context):
        allocation = self.viewport.get_allocation()
        self.drawing_area.set_size_request(
                allocation.width, allocation.height)
        Gdk.cairo_set_source_pixbuf(cairo_context, self.pixbuf, 0, 0)
        cairo_context.paint()

    def scale_image(self, widget, context, window):
        size = widget.get_allocation()
        if (size.width, size.height)==self.current_size:
            return
        self.current_size = (size.width, size.height)

        print(self.current_size)
        scaled = self.pixbuf.scale_simple(
                size.width,
                size.height,
                GdkPixbuf.InterpType.BILINEAR,
                )
        self.image.set_from_pixbuf(scaled)

    def on_button1_clicked(self, widget):
        print("Hello")

    def on_button2_clicked(self, widget):
        print("Goodbye")


def main():
    win = RostrumWindow()
    win.connect("destroy", Gtk.main_quit)
    Gtk.main()

if __name__=='__main__':
    main()
