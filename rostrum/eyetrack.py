import rostrum
import math
import re

# Override as you wish

face_id = 'face'
pupils_id = 'pupils'
eye_radius = 30
pupils_speed = 10

##############################

# Internal stuff

eyetrack_points = None

face_item = None
pupils_item = None
eye_point_offset = None
eye_position = None
pupils_have = None
pupils_want = None
now_tracking = None

TEMP = 0

PREFIX = 'eyetrack-'

TRANSLATE = re.compile(r'translate\((.*)\)')

##############################

def _get_points(
        item,
        bywords,
        ):

    name = item['id'][len(PREFIX):]
    result = {}

    if name in bywords:
        byword = bywords[name]
        desc = byword.phrase

        times = [byword.share[0]]

        for share in byword.share[1:]:
            times.append(share + times[-1])
    else:
        byword = None
        desc = name

        times = [int(float(name)*rostrum.util.FPS)]

    if item.name in ['circle', 'ellipse']:

        result[times[0]] = (
                float(item['cx']),
                float(item['cy']),
                name,
                )

    elif item.name=='path':

        points = []
        moves = item['d'].split(' ')

        if moves[0] in ('m', 'M'):
            moves.pop(0)

        i = 0
        for move in moves:

            if len(move)==1:
                raise ValueError(f"{move} is in this path and "
                        "I can't deal with it")
            x,y = [float(n) for n in move.split(',')]
            result[times[i]] = (x,y, f"{desc} {i}")

            i += 1
            if i>=len(times):
                break

    else:
        raise ValueError(f"Don't know how to handle {item.name}")

    return result

def load_points(ros):

    global eyetrack_points

    if eyetrack_points is not None:
        return

    rostrum.bywords.load_groups(ros)

    bywords = dict([
        (bw.name, bw) for bw in 
        rostrum.bywords.bywords_groups])

    eyetrack_items = [tag for tag in ros.soup.find_all()
            if tag.get('id','').startswith(PREFIX)]

    eyetrack_points = {}

    for et in eyetrack_items:
        eyetrack_points |= _get_points(item=et, bywords=bywords)

    print(eyetrack_points)

def adjust_pupils(ros):
    global pupils_item, pupils_have
    pupils_item['transform'] = f"translate{pupils_have}"
    print(f"-- eyes: adjusted pupils to {pupils_have}")

def move_eyetrack(frame, ros):

    global eyetrack_points, face_item, pupils_item, eye_point_offset
    global pupils_have, pupils_want, pupils_speed, now_tracking

    if eyetrack_points is None:
        load_points(ros)

    if pupils_item is None:
        pupils_item = ros.soup.find(id=pupils_id)

    if face_item is None:
        face_item = ros.soup.find(id=face_id)
        width = float(face_item['width'])
        height = float(face_item['height'])
        eye_point_offset = (
                width/2,
                height/3,
                )
        pupils_want = pupils_have = (0,0)
        adjust_pupils(ros)

    if pupils_want != pupils_have:

        angle = math.atan2(
            pupils_want[0]-pupils_have[0],
            pupils_want[1]-pupils_have[1],
            )

        distance = math.sqrt(
                (pupils_have[0]-pupils_want[0])**2 +
                (pupils_have[1]-pupils_want[1])**2
                )

        distance = min(distance, pupils_speed)

        pupils_have = (
                pupils_have[0]+math.sin(angle)*distance,
                pupils_have[1]+math.cos(angle)*distance,
                )

        adjust_pupils(ros)

    if frame not in eyetrack_points:
        return

    #global TEMP
    #TEMP += 0.1
    #point = ros.raster_to_svg_coords((
    #        1920+(500*math.sin(TEMP)),
    #        1080+(500*math.cos(TEMP)),
    #        ))
    #
    #point = ( *point, "TEMP", )

    # we have a new thing to look at

    point = eyetrack_points[frame]

    eye_point_x = float(face_item['x'])+eye_point_offset[0]
    eye_point_y = float(face_item['y'])+eye_point_offset[1]

    f = face_item
    while f.name!='svg':
        match = TRANSLATE.match(f.get('transform',''))
        if match:
            fields = [float(n) for n in match.groups(1)[0].split(',')]
            eye_point_x += fields[0]
            eye_point_y += fields[1]
        f = f.parent

    eye_point = ( eye_point_x, eye_point_y )

    angle = -math.atan2(
        point[1]-eye_point[1],
        point[0]-eye_point[0],
        )+(math.pi/2)

    print(f"-- eyes now tracking: {point}")

    pupils_want = (
            math.sin(angle)*eye_radius,
            math.cos(angle)*eye_radius,
            )

    if now_tracking is None:
        circle = ros.soup.new_tag('circle')
        circle['id'] = 'now-tracking'
        circle['r'] = '30'
        circle['style'] = (
                'stroke-width:20px; '
                'stroke:#ff0; '
                'stroke_opacity: 0.6; '
                'fill: none; '
                )

        text = ros.soup.new_tag('text')
        text['id'] = 'now-tracking-name'
        text['xml:space'] = 'preserve'
        text['style'] = (
                'fill:#ff0; '
                )

        line = ros.soup.new_tag('line')
        line['id'] = 'now-tracking-line'
        line['style'] = (
                'stroke-width:10px; '
                'stroke:#ff0; '
                'stroke_opacity: 0.6; '
                'font-weight: bold; '
                'font-size: 20pt; '
                'font-family: monospace; '
                )
        layer = face_item
        while layer.tag!='svg' and layer.get('inkscape:groupmode','')!='layer':
            layer = layer.parent
        layer.append(circle)
        layer.append(text)
        layer.append(line)

        now_tracking = (circle, text, line)

    now_tracking[0]['cx'] = point[0]
    now_tracking[0]['cy'] = point[1]
    now_tracking[1].string = point[2]
    now_tracking[1]['x'] = point[0]
    now_tracking[1]['y'] = point[1]
    now_tracking[2]['x1'] = eye_point[0]
    now_tracking[2]['y1'] = eye_point[1]
    now_tracking[2]['x2'] = point[0]
    now_tracking[2]['y2'] = point[1]
