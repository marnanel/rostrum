def move_example(ros, frame):
    thing = ros['something']

    colour = ['red', 'green', 'blue'][frame%3]

    thing['style'] = f'fill-color:{colour}'
