import pytest
import rostrum
import rostrum.util
from rostrum.rostrum import Rostrum

def test_rostrum_instantiate():

    r = Rostrum(
        basename = 'test/simple',
        )
    assert r.first_frame == 0
    assert r.last_frame == 2*rostrum.util.FPS

    r = Rostrum(
        basename = 'test/simple',
        frames = '2',
        )
    assert r.first_frame == 2
    assert r.last_frame == 2

    r = Rostrum(
        basename = 'test/simple',
        frames = '2-4',
        )
    assert r.first_frame == 2
    assert r.last_frame == 4

def test_rostrum_at():
    
    def run(expect_mover_id, **kwargs):
        r = Rostrum(
            basename = 'test/simple',
            )

        r.at(**kwargs)

        assert len(r.movers)==1
        mover = r.movers[expect_mover_id]

        assert mover.ros == r
        assert mover.item['id'].startswith(expect_mover_id[0])
        assert mover.attr == expect_mover_id[1]

    run(
            expect_mover_id = ('one', 'x'),
            when = 0,
            id = 'one:x',
            value = 100,
            )

    run(
            expect_mover_id = ('one', 'x'),
            when = 0,
            id = 'one',
            value = 100,
            attr = 'x'
            )

def test_rostrum_getitem():
    r = Rostrum(
        basename = 'test/simple',
        )
    assert r['one']['id'].startswith('one')
