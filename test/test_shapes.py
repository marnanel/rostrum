import rostrum
from rostrum.rostrum import Rostrum
from rostrum.shapes import *

def test_shapes():
    ros = Rostrum(
        basename = 'test/simple',
        )
    
    shapes = Shapes(ros=ros)

    one = shapes.one

    assert one.ros == ros
    assert one.shape == 'one'

    x = one.x

    assert x.ros == ros
    assert x.attr == 'x'

# FIXME we need to test x['#foo'], but that needs ros.at() to be testable
