import pytest
import rostrum
import rostrum.sprite
from rostrum.rostrum import Rostrum
from rostrum.mover import *

def points_to_list(points):
    result = []
    current = None
    for i in range(min(points), max(points)+1):
        if i in points:
            current = float(points[i])
        result.append(current)
    return result

def _simple_mover(
        shape = 'one',
        attr = 'x',
        initial_attr_value = None,
        ):

    ros = Rostrum(
        basename = 'test/simple',
        )

    if initial_attr_value is not None:
        rostrum.sprite.Sprite.maybe_wrap(
                        ros=ros,
                        something=ros[shape],
                        )[attr] = initial_attr_value
    
    mover = Mover(
            ros = ros,
            id = shape,
            attr = attr,
            )

    return mover

def test_mover():
    mover = _simple_mover()
    assert mover.points=={}

def test_mover_jump():
    mover = _simple_mover()
    mover.add_point( 0, 10)
    mover.add_point(10, 20, 'jump')
    assert mover.points=={0:'10', 10:'20'}

def test_mover_sine():
    mover = _simple_mover()
    mover.add_point( 0, 10)
    mover.add_point(10, 20, 'sine')
    ptl = points_to_list(mover.points)

    previous = None
    for i in range(len(ptl)-1):
        dy = round(ptl[i+1]-ptl[i], 2)

        assert dy > 0
        if previous is not None:
            assert dy < previous, i
        previous = dy

def test_mover_overshoot():
    mover = _simple_mover()
    mover.add_point( 0, 10)
    mover.add_point(10, 20, 'overshoot')
    ptl = points_to_list(mover.points)

    previous = None
    going_up = True
    for i in range(len(ptl)-1):
        dy = round(ptl[i+1]-ptl[i], 2)

        if dy > 0:
            assert going_up
        else:
            going_up = False

        if previous is not None:
            assert dy < previous, i

        previous = dy

    assert not going_up

def test_mover_linear():
    mover = _simple_mover()
    mover.add_point( 0, 10)
    mover.add_point(10, 20, 'linear')
    ptl = points_to_list(mover.points)

    previous = None
    for i in range(len(ptl)-1):
        dy = round(ptl[i+1]-ptl[i], 2)

        if previous is not None:
            assert dy == previous, i
        previous = dy

"""
@pytest.xfail()
def test_mover_with_initial_value():
    mover = _simple_mover(
            initial_attr_value = 7,
            )
    assert mover.points=={0: 7}
    """
