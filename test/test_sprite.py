import pytest
import rostrum
import rostrum.util
from rostrum.rostrum import Rostrum
from rostrum.sprite import Sprite

def test_sprite_simple():
    ros = Rostrum(
        basename = 'test/simple',
        )
 
    one = Sprite(
            ros=ros,
            source=ros['one'],
            )
    assert one._cel=='1'

def test_sprite_from_name():
    ros = Rostrum(
        basename = 'test/simple',
        )
 
    two = Sprite.from_name(
            ros=ros,
            name='two',
            )

    assert two['id'].startswith('two')

    for name in [
            'two',
            'two-scale',
            'two-rotate',
            'two-translate',
            ]:
        assert ros.soup.find(id=name) is not None, name
